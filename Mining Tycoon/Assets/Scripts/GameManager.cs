﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int MoneyInWallet = 1000;

    public int SecondsLeftInRound;

    public int LevelSecondsToStart = 300;

    // Start is called before the first frame update
    void Start()
    {    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartLevel()
    {
        SecondsLeftInRound = LevelSecondsToStart;
    }
}
