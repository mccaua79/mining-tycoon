﻿using CodeMonkey.Utils;
using UnityEngine;

public class Grid 
{
    private readonly int _width;
    private readonly int _height;
    private readonly float _cellSize;
    private readonly Vector3 _originPosition;
    private readonly int[,] _gridArray;
    private readonly TextMesh[,] _debugTextArray;

    public Grid(int width, int height, float cellSize, Vector3 originPosition)
    {
        _width = width;
        _height = height;
        _cellSize = cellSize;
        _originPosition = originPosition;
        _gridArray = new int[width, height];
        _debugTextArray = new TextMesh[width, height];

        for (var x = 0; x < _gridArray.GetLength(0); x++)
        {
            for (var y = 0; y < _gridArray.GetLength(1); y++)
            {
                _debugTextArray[x,y] = UtilsClass.CreateWorldText(_gridArray[x, y].ToString(), null, GetWorldPosition(x, y) + new Vector3(cellSize, cellSize) * 0.5f, 8, Color.white,
                    TextAnchor.MiddleCenter);
                Debug.DrawLine(GetWorldPosition(x,y), GetWorldPosition(x,y+1), Color.white, 100f);
                Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x+1, y), Color.white, 100f);
            }
        }

        Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width ,height), Color.white, 100f);
        Debug.DrawLine(GetWorldPosition(width, 0), GetWorldPosition(width, height), Color.white, 100f);
    }

    public void SetValue(int x, int y, int value)
    {
        if (x >= 0 && y >= 0 && x < _width && y < _height)
        {
            _gridArray[x, y] = value;
            _debugTextArray[x, y].text = _gridArray[x, y].ToString();
        }
    }

    public void SetValue(Vector3 worldPosition, int value)
    {
        GetXY(worldPosition, out var x, out var y);
        SetValue(x, y, value);
    }

    public int GetValue(int x, int y)
    {
        if (x >= 0 && y >= 0 && x < _width && y < _height)
        {
            return _gridArray[x, y];
        }

        return 0;
    }

    public int GetValue(Vector3 worldPosition)
    {
        GetXY(worldPosition, out var x, out var y);
        return GetValue(x, y);
    }

    public void HighlightCell(int x, int y)
    {
        if (x >= 0 && y >= 0 && x < _width && y < _height)
        {
            // TODO: show the highlight block
            Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x, y + 1), Color.yellow, 100f);
            Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x + 1, y), Color.yellow, 100f);
            Debug.DrawLine(GetWorldPosition(x + 1, y), GetWorldPosition(x + 1, y + 1), Color.yellow, 100f);
            Debug.DrawLine(GetWorldPosition(x, y + 1), GetWorldPosition(x + 1, y + 1), Color.yellow, 100f);
        }
    }
    public void HighlightCell(Vector3 worldPosition)
    {
        GetXY(worldPosition, out var x, out var y);
        HighlightCell(x, y);
    }

    private void GetXY(Vector3 worldPosition, out int x, out int y)
    {
        x = Mathf.FloorToInt((worldPosition - _originPosition).x / _cellSize);
        y = Mathf.FloorToInt((worldPosition - _originPosition).y / _cellSize);
    }

    private Vector3 GetWorldPosition(int x, int y)
    {
        return new Vector3(x,y) * _cellSize + _originPosition;
    }

}
