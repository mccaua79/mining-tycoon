﻿using UnityEngine;
using CodeMonkey.Utils;

public class Testing : MonoBehaviour
{
    private Grid _grid;
    // Start is called before the first frame update
    void Start()
    {
        _grid = new Grid(8, 1, 1f, new Vector3(-7.83f,1.21f));
    }


    // Update is called once per frame
    private void Update()
    {
        var mousePosition = UtilsClass.GetMouseWorldPosition();
        _grid.HighlightCell(mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            _grid.SetValue(mousePosition,56);
        }

        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log(_grid.GetValue(mousePosition));
        }
    }
}
