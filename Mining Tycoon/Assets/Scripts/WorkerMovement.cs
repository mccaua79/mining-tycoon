﻿using UnityEngine;

public class WorkerMovement : MonoBehaviour
{
    public float Speed;

    public Vector3 TargetPosition;
    public float NearToPositionTolerance = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        TargetPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var rawDestination = new Vector3(mousePosition.x, mousePosition.y, 0);
            TargetPosition = rawDestination;
            Debug.Log($"Take player to {TargetPosition} from {transform.position}");
        }

        if (Mathf.Abs(Vector3.Distance(TargetPosition, transform.position)) > NearToPositionTolerance)
        {
            var destination = Vector3.MoveTowards(transform.position, TargetPosition, Time.deltaTime * Speed);
            transform.position = destination;
        }

        // TODO: Create statuses (Idle, Walking, Mining)
        // TODO: Create payloadcurrent, and payload limit (to show how much they have mined)
        // TODO: Create a target objective (look at code monkey? and how he made the resource gathering)
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        // TODO: If colliding with rocks, then begin mining
    }
}
